close all;
clear;
clc
format long
%%
dataset = 'D:\workspace\Event Driven Computing\SSA\dohuan-code-star-track-5ffef60c2bb3\data\YanagisawaImages\Debris-6S\';
Yana.numOfObservations=10;
Yana.numOfShotsPerObv=29;
files=dir([dataset '*.fit']);

% if (length(files)~=Yana.numOfObservations*Yana.numOfShotsPerObv)
%     error('Number of files is not consistent\n');
% end

numset = Yana.numOfObservations;
trackLimit=29; % number of frames over 29 (total frames in one obv from TAOS)

a=1:length(files);
a=reshape(a,Yana.numOfShotsPerObv,[]);
frameIx=a(1:trackLimit,:);

C = 1; % brightness change threshold in log
epsilon = 0.001; % for safe log
time_between_frames = 2.9; %microseconds
eps = (10^6); %events per second in microseconds precision
preAllocLen = 100000000;
ROI_TAOS=[79 16 2076-79 2013-16]; % region of interest [start_X _start_Y height width]
mkdir('data');
mkdir('data',string(C));
for i=1:numset
    fprintf('Processing set: %d/%d\n',i,numset);
    time1=tic;
    events=zeros(preAllocLen,4);
    eventLen=0;
    for j=1:trackLimit
        fprintf('%f %f\n',i,j);
        dataIN = [dataset files(frameIx(j,i)).name];
      
        % --- Crop to remove hardware coverage in TAOS telescope
        image=fitsread(dataIN);
        ROIimg = imcrop(image,ROI_TAOS);
        log_ROIimg=log2(ROIimg+epsilon);
        
        if j==1
            [m n] = size(ROIimg);
            y=sub2ind([m n],m,n);
        end
        
        if j~=1
            for x = 1:y
                if (log_ROIimg(x)~=prev_log_ROIimg(x))
                    [startIndexCrossing,endIndexCrossing]= getCrossings(log_ROIimg(x),prev_log_ROIimg(x),C);
                    
                    polarity = log_ROIimg(x)>prev_log_ROIimg(x);
                    if ((startIndexCrossing+1)<=endIndexCrossing)
                        crossingIndex = (startIndexCrossing+1):endIndexCrossing;
                        brightnessCrossing = crossingIndex*C;
                        eventTiming = getCorrespondingEventTiming(log_ROIimg(x),prev_log_ROIimg(x),brightnessCrossing,j,eps,time_between_frames);
                        [p,q]=ind2sub([m n],x);
                        events((eventLen+1):(eventLen+length(eventTiming)),:)=[eventTiming' p*ones(length(eventTiming),1) q*ones(length(eventTiming),1) polarity*ones(length(eventTiming),1)];
                        eventLen=eventLen+length(eventTiming);
                    end
                end
            end
        end
        
        prev_log_ROIimg = log_ROIimg;
    end
    fprintf('Time: %f sec. Total events %f\n',toc(time1),eventLen);
    
    if eventLen>preAllocLen
        fprintf('More events %f than allocated\n',eventLen);
    end
    
    eventList=sortrows(events(1:eventLen,:),1);
    %csvwrite(strcat(string(i),'_event.txt'),eventList);
    dlmwrite(strcat('data\',string(C),'\',string(i),'_event.txt'),eventList,'delimiter',',','precision','%.6f')
end    