close all;
clear;
clc
format long
%%
filename = '1_event.txt';

events=dlmread(filename);

polarity=events(:,4);
positives = polarity>0.1;

positiveEvents = events(positives,1:3);

timeModel = KDTreeSearcher(positiveEvents(:,1));
XModel = KDTreeSearcher(positiveEvents(:,2));
YModel = KDTreeSearcher(positiveEvents(:,3));

timeResults=rangesearch(timeModel,positiveEvents(:,1),1,'Distance','cityblock');
XResults=rangesearch(XModel,positiveEvents(:,2),400,'Distance','cityblock');
YResults=rangesearch(YModel,positiveEvents(:,3),400,'Distance','cityblock');