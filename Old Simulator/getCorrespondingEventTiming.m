function [eventTiming] = getCorrespondingEventTiming(new_value,prev_value,brightnessCrossing,j,eps,time_between_frames)
%   Detailed explanation goes here
    
    slope = (new_value-prev_value)/time_between_frames;
    relativeTime=(brightnessCrossing-prev_value)/slope;
    actualTime = (j-1)*time_between_frames + relativeTime;
    eventTiming = floor(actualTime*eps);
    eventTiming = eventTiming/eps;
end

