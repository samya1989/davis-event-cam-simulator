function [startIndexCrossing,endIndexCrossing] = getCrossings(new_value,prev_value,C)
%   Detailed explanation goes here

    if new_value>prev_value
        smaller_value = prev_value;
        bigger_value = new_value;
    else
        smaller_value = new_value;
        bigger_value = prev_value;
    end
    
    startIndexCrossing = floor(smaller_value/C);
    endIndexCrossing = floor(bigger_value/C);
    
end