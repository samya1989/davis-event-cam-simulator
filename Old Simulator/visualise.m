close all;
clear;
clc;

filename='1_event.txt';
events=dlmread(filename);

gap = 5;
starting=1;
ending=length(events);
series= starting:gap:ending;

time=events(series,1)';
x=events(series,2)';
y=events(series,3)';
color=events(series,4)';
ccolor = color<=0;
%set(gcf,'Renderer','OpenGL');
set(gcf, 'DefaultFigureRenderer', 'opengl');
plot3(x(ccolor),y(ccolor),time(ccolor),'r*',x(~ccolor),y(~ccolor),time(~ccolor),'b*');
%plot3(x(ccolor),y(ccolor),time(ccolor),'r*');
%plot3(x(~ccolor),y(~ccolor),time(~ccolor),'b*');