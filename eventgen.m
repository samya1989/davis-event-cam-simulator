clear all;
close all;
clc;
format long;

width=240;
height=180;

repeatCount=60000;
pointThreshold = 0.999;
noiseThreshold = 0.9999995;
minPointCount = 3;
variableVelocityCount = 1;

%timeslabs in 1000 microsecs
minTimeSlab = 5;
maxTimeSlab = 100;

%angular rotation speed (degree) in seconds
min_theta = 2;
max_theta = 8;

K=[200 0 120;0 200 90; 0 0 1];
Kinv = K^-1;

h5File = 'RotDataset_ang2_8_time5k_100k.h5';

for r=1:repeatCount
    
    initPoints = rand(height,width)>pointThreshold;
    [rows,cols]=find(initPoints>0);
    onPixels = [cols,rows]';
    [m,n]=size(onPixels);
   
    if(n<minPointCount)
        fprintf('%d Discarding: Generated %d points\n',r,n)
        repeatCount=repeatCount+1;
        continue;
    end
    
    timeslab = minTimeSlab + rand*(maxTimeSlab-minTimeSlab);
    timeslab = round(timeslab*1000);
    
    fprintf('%d Generated %d points with timeslab %d\n',r,n,timeslab)

    onPixels3D = Kinv*[onPixels;ones(1,n)];
    onPixels3D_unorm = onPixels3D./vecnorm(onPixels3D);

    onPixels_last = onPixels;
    events = [];
    
    for sp=1:variableVelocityCount
        %rot_axis = rand(1,3);
        %rot_axis = rot_axis./vecnorm(rot_axis);
        [x,y,z] = sph2cart(rand*2*pi, -pi/2*(1-2*rand), 1);
        rot_axis = [x,y,z];
        rot_theta = min_theta + rand*(max_theta-min_theta);
        fprintf('Generated rotation %d,%d,%d\n',rotm2eul(axang2rotm([rot_axis rot_theta])))
        
        rot_theta_microsec = rot_theta/1000000;
        rot_p(sp,:) = [rot_axis rot_theta_microsec];
    
        for t=1:timeslab
            
            if sp==1
                R = axang2rotm([rot_p(1,1:3) rot_p(1,4)*t]);
            end
            
            if sp==2
                R1 = axang2rotm([rot_p(1,1:3) rot_p(1,4)*timeslab]);
                R2 = axang2rotm([rot_p(2,1:3) rot_p(2,4)*t]);
                R=R2*R1;
            end
                
            noise = rand(height,width)>noiseThreshold;
            [nrows,ncols]=find(noise>0);

            onPixels_rot = K*R*onPixels3D_unorm;
            onPixels_rot = onPixels_rot./onPixels_rot(3,:);
            onPixels_rot = round(onPixels_rot(1:2,:));

            pixelsChange = onPixels_last - onPixels_rot;
            [rows,cols] = find(pixelsChange~=0);
            fire = unique([rows,cols],'rows');

            new_events = onPixels_rot(:,fire(:,2));
            new_events = new_events(:,(new_events(1,:)>0 & new_events(1,:)<width));
            new_events = new_events(:,(new_events(2,:)>0 & new_events(2,:)<height));
            [p,q]=size(new_events);

            events = [events;new_events',((sp-1)*timeslab+t)*ones(q,1),ones(q,1)];
            events = [events;[ncols,nrows,((sp-1)*timeslab+t)*ones(length(ncols),1),zeros(length(ncols),1)]];

            onPixels_last = onPixels_rot;
        end
    end
    
    
%     size(events)
%     plot3(events(:,1),events(:,2),events(:,3),'r*')
%     pause(3);

    dbname = ['/rotations',num2str(r)];
    labelname = ['/labels',num2str(r)];
    [e1,e2]=size(events);
    [r1,r2]=size(rot_p);
    
    h5create(h5File,dbname,[e1,e2]);
    h5write(h5File,dbname,events);
    h5create(h5File,labelname,[r1,r2]);
    h5write(h5File,labelname,rot_p);
    
end