clear all;
close all;
clc;
format long;

width=240;
height=180;

timeslab=20000;%microseconds
repeatCount=10000;
theta_range = 4;%angular rotation speed (degree) in seconds
pointThreshold = 0.999;
noiseThreshold = 0.9999995;
minPointCount = 20;
minEventCount = 2048;

K=[200 0 120;0 200 90; 0 0 1];
Kinv = K^-1;

h5File = 'RotDataset2048.h5';

for r=1:repeatCount
    
    rot_axis = rand(1,3);
    rot_axis = rot_axis./vecnorm(rot_axis);
    
    rot_theta = rand*theta_range;
    rot_theta_microsec = rot_theta/100000;
    rot_p = [rot_axis rot_theta_microsec];
    
    initPoints = rand(height,width)>pointThreshold;
    [rows,cols]=find(initPoints>0);
    onPixels = [cols,rows]';
    [m,n]=size(onPixels);

    if(n<minPointCount)
        fprintf('%d Discarding: Generated %d points\n',r,n)
        continue;
    end
    
    fprintf('%d Generated %d points\n',r,n)

    onPixels3D = Kinv*[onPixels;ones(1,n)];
    onPixels3D_unorm = onPixels3D./vecnorm(onPixels3D);

    onPixels_last = onPixels;
    events = [];
    
    for t=1:timeslab
        R = axang2rotm([rot_axis rot_theta_microsec*t]);
        
        noise = rand(height,width)>noiseThreshold;
        [nrows,ncols]=find(noise>0);
        
        onPixels_rot = K*R*onPixels3D_unorm;
        onPixels_rot = onPixels_rot./onPixels_rot(3,:);
        onPixels_rot = round(onPixels_rot(1:2,:));
        
        pixelsChange = onPixels_last - onPixels_rot;
        [rows,cols] = find(pixelsChange~=0);
        fire = unique([rows,cols],'rows');
              
        new_events = onPixels_rot(:,fire(:,2));
        new_events = new_events(:,(new_events(1,:)>0 & new_events(1,:)<width));
        new_events = new_events(:,(new_events(2,:)>0 & new_events(2,:)<height));
        [p,q]=size(new_events);
       
        events = [events;new_events' t*ones(q,1)];
        events = [events;[ncols,nrows,t*ones(length(ncols),1)]];
        
        onPixels_last = onPixels_rot;
    end
    
    [m,n] = size(events);
    if(m<minEventCount)
        continue
    end
    
    p = randperm(m,minEventCount);
    events=events(p,:);
    
%     size(events)
%     plot3(events(:,1),events(:,2),events(:,3),'r*')
%     pause(3);

    dbname = ['/rotations',num2str(r)];
    labelname = ['/labels',num2str(r)];
    [e1,e2]=size(events);
    [r1,r2]=size(rot_p);
    
    h5create(h5File,dbname,[e1,e2]);
    h5write(h5File,dbname,events);
    h5create(h5File,labelname,[r1,r2]);
    h5write(h5File,labelname,rot_p);
    
end